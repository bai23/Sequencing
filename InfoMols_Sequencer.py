import csv
import IsoSpecPy as iso

# Sets the maximum length oliogmer to look for

maxlength = 6

# Sets the value of the "Sensitivity" Parameter, N

N = 3

Confidence = []

# Input the name of the HRMS peak-list, saved as a tab-delimited text file

HRMS_csv_file = "A5D1-2_HRMSpeaks.txt"

# Defines the Chemical Formulae for all the motifs used in the oligomers

A_form = "C16H29Cl2N4OP"
D_form = "C14H16Cl2N4O"
Piperazine_form = "C4H10N2"
Piperidine = "C5H11N"
AzidePip = "C5H10N4"
AlkynePip = "C7H11N"
Benzylamine = "C7H9N"
NMethylPip = "C5H12N2"

# Imports the HRMS data from the text file
# Creates lists of all the peaks present, and their respective intensities 

with open(HRMS_csv_file, newline='') as csvfile:
    HRMScsv = csv.reader(csvfile, delimiter='\t')
    HRMS_mz = []
    HRMS_mz_intensity = []
    for row in HRMScsv:
        HRMS_mz.append(float(row[0]))
        HRMS_mz_intensity.append(float(row[1]))  

# Counts the number of As and Ds in a sequence

def AD_counter(sequence):
    result = [0,0]
    for i in sequence:
        if i == "A":
            result[0] += 1
        if i == "D":
            result[1] += 1
    return(result)

# Performs the third generation scoring on an inputted m/z value

def MassSearch(mz):
    x = 0
    ppmx = 1000
    intx = 0
    scorex = 0
    for i in HRMS_mz:
        ppm = max(abs(mz - i) * 1000000 / mz, 5)
        if ppm < ppmx:
            int = HRMS_mz_intensity[HRMS_mz.index(i)]
            scorex = int / pow(ppm, N)
            x = i
            ppmx = ppm
    if x != 0:
        intx = HRMS_mz_intensity[HRMS_mz.index(x)]
        return(scorex)
    else:
        return(0)

# Given the mass of a fragment, finds the mean score of all the proton-adducted species. Also looks for different isotopes too.

def MassSearch123(MASS):
    result = []
    for j in [0, 1.008665]:
        for i in range(10):
            x = (MASS + j)/(i+1) + 1.007276
            if x > 350 and x < 3000:
                result.append(MassSearch(x))
    if len(result) != 0:
        return(sum(result) / len(result))
    else:
        return(0)

# Input Chemical Formula as a string eg "C2H4", outputs the mass of the most abundant isotope
# credit: Analytical Chemistry 2020 92 (14), 9472-9475

def MolMass(xxx):
    sp = iso.IsoTotalProb(formula = xxx, prob_to_cover = .99)
    masslist = []
    problist = []
    for mass, prob in sp:
        masslist.append(mass)
        problist.append(prob)
    return(round(masslist[problist.index(max(problist))], 5))

# Input a Chemical formula and it will output the formula as a dictionary; needed for converting oligomer sequences "AADAD" into chemical formulae

def FormulaToDict(formula):
    result = {"C": 0, "H": 0, "O": 0, "N": 0, "P": 0, "X": 0}
    formula = formula[1:].replace("Cl", "X")
    i = 1
    x = "C"
    while i < len(formula):
        if formula[i].isdigit() == False:
            if formula.split(formula[i])[0] == "":
                result[x] = 1
            else:
                result[x] = int(formula.split(formula[i])[0])
            x = formula[i]
            formula = formula.split(formula[i])[1]
            i = 0
        else:
            i += 1
    if formula == "":
        result[x] = 1
    else:
        result[x] = formula
    
    return(result)

# Input a dictionary and it will output a chemical formula; needed for converting oligomer sequences "AADAD" into chemical formulae

def DictToFormula(dict):
    result = ""
    for i in ["C", "H", "O", "N", "P", "X"]:
        if dict.get(i) != 0:
            result += i
            if dict[i] != 1:
                result += str(dict[i])
    return(result)

A_dict = FormulaToDict(A_form)
D_dict = FormulaToDict(D_form)
p_dict = FormulaToDict(Piperazine_form)

# Using the MolMass function and the Dictionary - Formulae conversions to calculate the mass of an oligomer given its sequence and capping groups

def MassCalc(seq, c1, c2):
    c1_dict = FormulaToDict(c1)
    c2_dict = FormulaToDict(c2)
    Acount = AD_counter(seq)[0]
    Dcount = AD_counter(seq)[1]
    hcl = {"C": 0, "H": 1, "O": 0, "N": 0, "P": 0, "X": 1}
    result = {"C": 0, "H": 0, "O": 0, "N": 0, "P": 0, "X": 0}
    for i in ["C", "H", "O", "N", "P", "X"]:
        result[i] = Acount * int(A_dict[i]) + Dcount * int(D_dict[i]) - 2 * (Acount + Dcount) * int(hcl[i]) + (Acount + Dcount - 1) * int(p_dict[i]) + int(c1_dict[i]) + int(c2_dict[i])
    return(MolMass(DictToFormula(result)))

# Define the capping groups and this will find the composition of the sequence in As and Ds, but not their order

def FindComp(cap1, cap2):
    x = 0
    result = []
    i = 0
    j = 0
    while i < maxlength + 1:
        while j < maxlength - i + 1:
            test = ["A" for p in range(i)] + ["D" for q in range(j)]
            mtest = MassCalc(test, cap1, cap2)
            score = MassSearch123(mtest)
            if score > x:
                result = test.copy()
                x = score
                print(test)
                print(score)
            j += 1
        i += 1
        j = 0
    return(result)

# Removes the elements of one sequence from another sequence

def Remove(seq1, seq2):
    result = []
    bases = ["A", "D"]
    for i in range(2):
        for j in range(AD_counter(seq1)[i]- AD_counter(seq2)[i]):
            result.append(bases[i])
    return(result) 

# Works out the sequence of the oliogmer, working from one end to the other

def FindSeq(emp, cap1, cap2):
    seq = []
    length = len(emp)
    for q in range(length):
        if AD_counter(Remove(emp, seq))[0] == 0 or AD_counter(Remove(emp, seq))[1] == 0:
            for i in Remove(emp,seq):
                seq.append(i)
        else:
            Ascore = MassSearch123(MassCalc(seq + ["A"], cap1, Benzylamine)) * MassSearch123(MassCalc(seq + ["A"], cap1, NMethylPip)) * MassSearch123(MassCalc(Remove(emp,seq + ["A"]), cap2, Benzylamine)) * MassSearch123(MassCalc(Remove(emp,seq + ["A"]), cap2, NMethylPip))
            Dscore = MassSearch123(MassCalc(seq + ["D"], cap1, Benzylamine)) * MassSearch123(MassCalc(seq + ["D"], cap1, NMethylPip)) * MassSearch123(MassCalc(Remove(emp,seq + ["D"]), cap2, Benzylamine)) * MassSearch123(MassCalc(Remove(emp,seq + ["D"]), cap2, NMethylPip))
            if Ascore > Dscore:
                seq.append("A")
                Confidence.append(Ascore / Dscore)
            if Ascore < Dscore:
                seq.append("D")
                Confidence.append(Dscore / Ascore)
    return(seq)

# Finds the sequence of the oligomer

def Sequence(cap1, cap2):
    X = 0
    comp = FindComp(cap1, cap2) # Find composition of oliogmer
    seq1 = FindSeq(comp, cap1, cap2) # Find the sequence working right to left
    seq2 = FindSeq(comp, cap2, cap1) # Find the sequence working left to right
    if seq1 == list(reversed(seq2)): # Check that the two sequences match
        X = min(Confidence) # Calculate overall confidence for the sequencing
        print("Best Sequence: Cap1 -", "".join(seq1), "- Cap2")
        print("Confidence value:", X)
    else:
        print("Error - sequences don't match")
    return()


Sequence(AlkynePip, AzidePip)


